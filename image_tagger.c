/*
* image_tagger.c
* source code from Lab 5/6 of COMP30023
* @author: Alexander D'Arcy Cock
*/

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <strings.h>
#include <sys/select.h>
#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// Constants for the HTTP response headers
static char const * const HTTP_200_FORMAT = "HTTP/1.1 200 OK\r\n\
Content-Type: text/html\r\n\
Content-Length: %ld\r\n\r\n";
static char const * const HTTP_200_COOKIE = "HTTP/1.1 200 OK\r\n\
Content-Type: text/html\r\n\
Content-Length: %ld\r\n\
Set-Cookie: player=%s\r\n\
Set-Cookie: nameLength=%d\r\n\r\n";
static char const * const HTTP_400 = "HTTP/1.1 400 Bad Request\r\nContent-Length: 0\r\n\r\n";
static int const HTTP_400_LENGTH = 47;
static char const * const HTTP_404 = "HTTP/1.1 404 Not Found\r\nContent-Length: 0\r\n\r\n";
static int const HTTP_404_LENGTH = 45;
// The html filenames are stored in constants
static char* const PAGE_1 = "1_intro.html";
static char* const PAGE_2 = "2_start.html";
static char* const PAGE_3 = "3_first_turn.html";
static char* const PAGE_4 = "4_accepted.html";
static char* const PAGE_5 = "5_discarded.html";
static char* const PAGE_6 = "6_endgame.html";
static char* const PAGE_7 = "7_gameover.html";

// represents the types of method
typedef enum {
    GET,
    POST,
    UNKNOWN
} METHOD;

// represents the states that the players can be in
typedef enum {
	WELCOME,
	WAITING,
	READY,
	COMPLETED,
	GAMEOVER
} GAMESTATE;

// represents the different attributes of each player
struct Player {
	char* name;
	int nameLength;
	int playerNum;
	GAMESTATE gameState;
	char* keywordList[100];
	int numKeywords;
};

// The players are global to access them from any function
static struct Player player1;
static struct Player player2;

//checks the output from the buff to fetch the url of the request
static char* getURL(char* curr) {
	char* url;
	if (strncmp(curr, "1_intro.html ", 13) == 0 || strncmp(curr, " HTTP/1.1", 9) == 0)
    {
        url = PAGE_1;
    } else if(strncmp(curr, "2_start.html ", 13) == 0)
    {
        url = PAGE_2;
    } else if(strncmp(curr, "3_first_turn.html ", 18) == 0 || strstr(curr, "?start=Start ") != NULL)
    {
        url = PAGE_3;
    } else if(strncmp(curr, "4_accepted.html ", 16) == 0)
    {
        url = PAGE_4;
    } else if(strncmp(curr, "5_discarded.html ", 17) == 0)
    {
        url = PAGE_5;
    } else if(strncmp(curr, "6_endgame.html ", 15) == 0)
    {
        url = PAGE_6;
    } else if(strncmp(curr, "7_gameover.html ", 16) == 0)
    {
        url = PAGE_7;
    } else {
		url = NULL;
	}
	return url;
}

// Handles storing the keywords and returns true if there is a match to an added keyword
static bool addKeyword(char * keyword, int playerNum) {
	int i = 0;
	bool match = false;
	if(playerNum == 1) {
		while(player2.keywordList[i] != NULL) {
			if((strcmp(player2.keywordList[i], keyword)) == 0) {
				match = true;
			}
			i++;
		}
		if((player1.keywordList[player1.numKeywords] = malloc(strlen(keyword)*sizeof(char))) == NULL) {
			printf("malloc error");
			exit(0);
		}
		memcpy(player1.keywordList[player1.numKeywords],keyword,strlen(keyword));
		player1.numKeywords++;
	} else {
		while(player1.keywordList[i] != NULL) {
			if((strcmp(player1.keywordList[i], keyword)) == 0) {
				match = true;
			}
			i++;
		}
		if((player2.keywordList[player2.numKeywords] = malloc(strlen(keyword)*sizeof(char))) == NULL) {
			printf("malloc error");
			exit(0);
		}
		memcpy(player2.keywordList[player2.numKeywords],keyword,strlen(keyword));
		player2.numKeywords++;
	}
	return match;
}

// Returns the player that the cookie in the request header refers to
static struct Player* checkCookie(char* buff) {
	char * cookie;
	if((cookie = strstr(buff,"player=")) != NULL) {
		if(strncmp(cookie+7,player1.name,player1.nameLength) == 0) {
			return &player1;
		} else if(strncmp(cookie+7,player2.name,player2.nameLength) == 0) {
			return &player2;
		} else if(player1.name == NULL) {
			char* length = strstr(buff,"nameLength=");
			player1.nameLength = atoi(length+11);
			if((player1.name = malloc(player1.nameLength*sizeof(char))) == NULL) {
					printf("malloc error");
					return false;
				}
			memcpy(player1.name,cookie+7,player1.nameLength);
			return &player1;
		} else if(player2.name == NULL) {
			char* length = strstr(buff,"nameLength=");
			player1.nameLength = atoi(length+11);
			if((player2.name = malloc(player2.nameLength*sizeof(char))) == NULL) {
					printf("malloc error");
					return false;
				}
			memcpy(player2.name,cookie+7,player2.nameLength);
			return &player2;
		}
	}
	return NULL;
}

// Forms the HTTP response to the client and returns true if one is sent
static bool sendResponse(int sockfd, char* file, char* post) {
	char buff[2049];
	int n;
	
	if(file == NULL) {
		if (write(sockfd, HTTP_404, HTTP_404_LENGTH) < 0){
			perror("write");
			return false;
		}
		return true;
	} else {
		struct stat st;
		stat(file, &st);
		if(post != NULL && (strncmp(post,"user=",5)) == 0) {
			if(player1.name == NULL) {
				char* temp = post+5;
				if((player1.name = malloc(strlen(temp)*sizeof(char))) == NULL) {
					printf("malloc error");
					return false;
				}
				memcpy(player1.name,temp,strlen(temp));
				player1.nameLength = strlen(player1.name);
				player1.gameState = WAITING;
				n = sprintf(buff, HTTP_200_COOKIE, st.st_size, player1.name, player1.nameLength);
			} else if (player2.name == NULL) {
				char* temp = post+5;
				if((player2.name = malloc(strlen(temp)*sizeof(char))) == NULL) {
					printf("malloc error");
					return false;
				}
				memcpy(player2.name,temp,strlen(temp));
				player2.nameLength = strlen(player2.name);
				player2.gameState = WAITING;
				n = sprintf(buff, HTTP_200_COOKIE, st.st_size, player2.name, player2.nameLength);
			} else {
				n = sprintf(buff, HTTP_200_FORMAT, st.st_size);
			}
		} else {
			n = sprintf(buff, HTTP_200_FORMAT, st.st_size);
		}
	}
    // send the header first
    if (write(sockfd, buff, n) < 0) {
        perror("write");
        return false;
    }
    // send the file
    int filefd = open(file, O_RDONLY);
    do {
        n = sendfile(sockfd, filefd, NULL, 2048);
    } while (n > 0);
    if (n < 0) {
        perror("sendfile");
        close(filefd);
        return false;
    }
    close(filefd);
	return true;
}

// Handles the different cases for get requests and returns true if a response is sent
static bool getRequest(int sockfd, char* buff, char* url, int n) {
	
	struct Player* player = checkCookie(buff);
	
	if(url != NULL && player != NULL) {
		if (strcmp(url,PAGE_1) == 0) {
			url = PAGE_2;
		}
		if (strcmp(url,PAGE_3) == 0 || strcmp(url,PAGE_6) == 0) {
			player->gameState = READY;
		}
	} else if(player1.gameState == GAMEOVER) {
		url = PAGE_7;
	}
	
	if(sendResponse(sockfd,url,NULL)) {
		return true;
	}
	return false;
}

// Handles the different cases for post requests and returns true if a response is sent
static bool postRequest(int sockfd, char* buff, char* url, int n) {
	char* post;
	char* file;
	
	if((post = strstr(buff,"user=")) != NULL) {
		file = PAGE_2;
	} else if((post = strstr(buff,"keyword=")) != NULL) {
		if(player1.gameState == player2.gameState && player1.gameState == READY) {
			struct Player* player = checkCookie(buff);
			if(addKeyword(post+8,player->playerNum)) {
				file = PAGE_6;
				player1.gameState = COMPLETED;
				player2.gameState = COMPLETED;
			} else {
				file = PAGE_4;
			}
			printf("P1 keywords: \n");
			for(int i = 0; i < player1.numKeywords; i++) {
				printf("%s\n",player1.keywordList[i]);
			}
			printf("P2 keywords: \n");
			for(int i = 0; i < player2.numKeywords; i++) {
				printf("%s\n",player2.keywordList[i]);
			}
		} else if (player1.gameState == COMPLETED && player2.gameState == COMPLETED) {
			file = PAGE_6;
		} else if(player1.gameState == GAMEOVER) {
			file = PAGE_7;
		} else {
			file = PAGE_5;
		}
	} else if((post = strstr(buff,"quit=Quit")) != NULL || player1.gameState == GAMEOVER) {
		file = PAGE_7;
		player1.gameState = GAMEOVER;
	} else {
		file = NULL;
	}
	
	if(strcmp(file,PAGE_6) == 0) {
		int i = 0;
		if(player1.numKeywords > 0) {
			for(i = 0; i < player1.numKeywords; i++) {
				free(player1.keywordList[i]);
			}
			player1.numKeywords = 0;
		}
		if(player2.numKeywords > 0) {
			for(i = 0; i < player2.numKeywords; i++) {
				free(player2.keywordList[i]);
			}
			player2.numKeywords = 0;
		}
	}
	
	if(sendResponse(sockfd,file,post)) {
		return true;
	}
	return false;
}

// Attempts to handle the http requests, returns true if a response is sent
static bool handle_http_request(int sockfd) {
    // try to read the request
    char buff[2049];
	char * url;
    int n = read(sockfd, buff, 2049);
    if (n <= 0)
    {
        if (n < 0)
            perror("read");
        else
            printf("socket %d close the connection\n", sockfd);
        return false;
    }
	printf("%s\n", buff);
	
    // terminate the string
    buff[n] = 0;
    char * curr = buff;

    // parse the method
    METHOD method = UNKNOWN;
    if (strncmp(curr, "GET ", 4) == 0)
    {
        curr += 4;
        method = GET;
    }
    else if (strncmp(curr, "POST ", 5) == 0)
    {
        curr += 5;
        method = POST;
    }
    else if (write(sockfd, HTTP_400, HTTP_400_LENGTH) < 0)
    {
        perror("write");
        return false;
    }

    // sanitise the URI
    while (*curr == '.' || *curr == '/') {
        ++curr;
    // assume the only valid request URI is "/" but it can be modified to accept more files
	}
	
	url = getURL(curr);
	if (method == GET) {
		if(getRequest(sockfd,buff,url,n) == false) {
			return false;
		}
	} else if (method == POST) {
		if(postRequest(sockfd,buff,url,n) == false) {
			return false;
		}
	} else {
	    // never used, just for completeness
		fprintf(stderr, "no other methods supported");
	}
	printf("P1: %d\nP2: %d\n",player1.gameState,player2.gameState);
    return true;
}

int main(int argc, char * argv[]) {
    if (argc < 3)
    {
        fprintf(stderr, "usage: %s ip port\n", argv[0]);
        return 0;
    }

    // create TCP socket which only accept IPv4
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // reuse the socket if possible
    int const reuse = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) < 0)
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // create and initialise address we will listen on
    struct sockaddr_in serv_addr;
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    // if ip parameter is not specified
    serv_addr.sin_addr.s_addr = inet_addr(argv[1]);
    serv_addr.sin_port = htons(atoi(argv[2]));

    // bind address to socket
    if (bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    // listen on the socket
    listen(sockfd, 5);

    // initialise an active file descriptors set
    fd_set masterfds;
    FD_ZERO(&masterfds);
    FD_SET(sockfd, &masterfds);
    // record the maximum socket number
    int maxfd = sockfd;
	
	char str[INET_ADDRSTRLEN];
	printf("image_tagger server is now running at IP: %s on port %d\n", inet_ntop(AF_INET, &serv_addr.sin_addr, str, INET_ADDRSTRLEN), ntohs(serv_addr.sin_port));
	
	player1.gameState = WELCOME;
	player2.gameState = WELCOME;
	player1.numKeywords = 0;
	player2.numKeywords = 0;
	player1.playerNum = 1;
	player2.playerNum = 2;
	
    while (1)
    {
        // monitor file descriptors
        fd_set readfds = masterfds;
        if (select(FD_SETSIZE, &readfds, NULL, NULL, NULL) < 0)
        {
            perror("select");
            exit(EXIT_FAILURE);
        }

        // loop all possible descriptor
        for (int i = 0; i <= maxfd; ++i)
            // determine if the current file descriptor is active
            if (FD_ISSET(i, &readfds))
            {
                // create new socket if there is new incoming connection request
                if (i == sockfd)
                {
                    struct sockaddr_in cliaddr;
                    socklen_t clilen = sizeof(cliaddr);
                    int newsockfd = accept(sockfd, (struct sockaddr *)&cliaddr, &clilen);
                    if (newsockfd < 0)
                        perror("accept");
                    else
                    {
                        // add the socket to the set
                        FD_SET(newsockfd, &masterfds);
                        // update the maximum tracker
                        if (newsockfd > maxfd)
                            maxfd = newsockfd;
                        // print out the IP and the socket number
                        char ip[INET_ADDRSTRLEN];
                        printf(
                            "new connection from %s on socket %d\n",
                            // convert to human readable string
                            inet_ntop(cliaddr.sin_family, &cliaddr.sin_addr, ip, INET_ADDRSTRLEN),
                            newsockfd
                        );
                    }
                }
                // a request is sent from the client
                else if (!handle_http_request(i))
                { 
                    close(i);
                    FD_CLR(i, &masterfds);
                }
            }
    }

	if(player1.name != NULL) {
		free(player1.name);
	}
	if(player2.name != NULL) {
		free(player2.name);
	}
	
    return 0;
}