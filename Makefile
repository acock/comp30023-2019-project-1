CC=gcc
CPPFLAGS=-Wall  -Werror -O3

SRC=image_tagger.c
TARGET=image_tagger

all: $(SRC)
	$(CC) -o $(TARGET) $(SRC) $(CPPFLAGS)

clean:
	rm -f $(TARGET) *.o